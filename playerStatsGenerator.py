import json

def saveStatsToFile(players):
    with open('playerStats.json', 'w') as file:
        json.dump(players, file, indent=4)

players_stats = [
    {"name": "123", "password": "123", "win": 1, "tie": 1, "loss": 1},
    {"name": "qwe", "password": "qwe", "win": 2, "tie": 0, "loss": 1}
]

saveStatsToFile(players_stats, 'playerStats.json')
