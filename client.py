import socket
import json
from tkinter import *
import threading

t = Tk()
t.resizable(False, False)

serverAddressPort = None
bufferSize = 1024
isLoggedIn = False
username = ""
password = ""

global currentPlayer, buttons

looking = False
lookingThread = None
cancelSearch = threading.Event()
clientSocket = None

def connectToServer(address):
    global clientSocket
    clientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    try:
        clientSocket.connect(address)
        print(f'Connected to server at {address}')
        return clientSocket 
    except ConnectionRefusedError:
        print("Connection refused. Server is not available.")
        clientSocket.close()
        return 'failed'
    except Exception as e:
        print(f"Error connecting to server: {e}")
        clientSocket.close()
        return 'failed'

def disconnectFromServer(clientSocket):
    try:
        clientSocket.close()
        print("Disconnected from server.")
    except Exception as e:
        print(f"Error disconnecting from server: {e}")

def logInClient(usernameInput, passwordInput):
    global username, password, isLoggedIn

    username = usernameInput
    password = passwordInput

    try:
        loginRequest = {
            "action": "login",
            "username": username,
            "password": password
        }
        clientSocket.sendall(json.dumps(loginRequest).encode())

        while True:
            try:
                response = clientSocket.recv(bufferSize).decode()
                print(f"Server response: {response}")
                responseJson = json.loads(response)
                if responseJson["status"] == "success":
                    isLoggedIn = True
                    break
                else:
                    if responseJson["message"] == "Invalid username or password":
                        print("Failed.")
                        isLoggedIn = False
                        break
            except json.JSONDecodeError:
                print("Failed to decode JSON response from server")
            except Exception as e:
                print(f"Error receiving server response: {e}")
                break

    except json.JSONDecodeError:
        print("Failed to decode JSON response from server")
    except Exception as e:
        print(f"Error during login: {e}")

def registerClient(username, password):
    try:
        registerRequest = {
            "action": "register",
            "username": username,
            "password": password
        }
        clientSocket.sendall(json.dumps(registerRequest).encode())
        response = clientSocket.recv(bufferSize).decode()
        responseJson = json.loads(response)
        print(f"Server response: {responseJson['message']}")
        if responseJson['message'] == 'Username already exists':
            return 'failure'
        else:
            return 'success'
    except json.JSONDecodeError:
        print("Failed to decode JSON response from server")
    except Exception as e:
        print(f"Error during registration: {e}")

def statsClient(username):
    try:
        registerRequest = {
            "action": "stats",
            "username": username,
        }
        clientSocket.sendall(json.dumps(registerRequest).encode())
        response = clientSocket.recv(bufferSize).decode()
        responseJson = json.loads(response)
        print(f"Server response: win: {responseJson['win']}, tie: {responseJson['tie']}, loss: {responseJson['loss']}")
        stats=[]
        stats.append(responseJson['win'])
        stats.append(responseJson['tie'])
        stats.append(responseJson['loss'])
        if responseJson['status'] == 'failure':
            return 'failure'
        else:
            return stats
    except json.JSONDecodeError:
        print("Failed to decode JSON response from server")
    except Exception as e:
        print(f"Error during fetching: {e}")

def findMatch():
    global isLoggedIn, username, looking, cancelSearch
    if not isLoggedIn:
        print("You must be logged in to find a match.")
        return

    try:
        matchRequest = {
            "action": "find_match",
            "username": username
        }
        clientSocket.sendall(json.dumps(matchRequest).encode())

        while looking and not cancelSearch.is_set():
            response = clientSocket.recv(bufferSize).decode()
            responseJson = json.loads(response)
            print(f"Server response: {responseJson['message']}")
            if responseJson["status"] == "success":
                if "board" in responseJson:
                    showGameScreen(responseJson["board"], responseJson["currentPlayer"], responseJson["secondPlayer"])
                break
    except json.JSONDecodeError:
        print("Failed to decode JSON response from server")
    except Exception as e:
        print(f"Error finding match: {e}")
    finally:
        looking = False

def leaveQueue():
    try:
        request = {
            "action": "leave_queue",
            "username": username
        }
        clientSocket.sendall(json.dumps(request).encode())
        print("Request to cancel match sent.")
    except Exception as e:
        print(f"Error sending cancel match request: {e}")

def checkWin(board, player):
    winningCombinations = [
        [0, 1, 2], [3, 4, 5], [6, 7, 8],
        [0, 3, 6], [1, 4, 7], [2, 5, 8],
        [0, 4, 8], [2, 4, 6]
    ]
    for combo in winningCombinations:
        if all(board[i] == player for i in combo):
            return True
    return False

def checkDraw(board):
    return all(cell != ' ' for cell in board)

def showLoginScreen():
    
    def logInUser():
        global isLoggedIn
        username = usernameInput.get()
        password = passwordInput.get()
        logInClient(username, password)
        
        if isLoggedIn == False:
            loginTitle.config(text='INVALID USERNAME\nOR PASSWORD')
        else:
            loginTitle.place_forget()
            usernameLabel.place_forget()
            usernameInput.place_forget()
            passwordLabel.place_forget()
            passwordInput.place_forget()
            loginBtn.place_forget()
            showLoggedInScreen()

    loginTitle = Label(t, text='PLEASE INPUT\nUSERNAME AND\nPASSWORD', font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    loginTitle.place(x=60, y=30, width=420, height=140)

    usernameLabel = Label(t, text='USERNAME', font=('Fixedsys', 24, 'bold'), fg='snow', bg='lightsalmon4')
    usernameLabel.place(x=115, y=200, width=300, height=70)
    usernameInput = Entry(t, font=('Fixedsys', 24))
    usernameInput.place(x=115, y=280, width=300, height=50)

    passwordLabel = Label(t, text='PASSWORD', font=('Fixedsys', 24, 'bold'), fg='snow', bg='lightsalmon4')
    passwordLabel.place(x=115, y=360, width=300, height=70)
    passwordInput = Entry(t, font=('Fixedsys', 24), show='*')
    passwordInput.place(x=115, y=440, width=300, height=50)

    loginBtn = Button(t, text='LOG IN', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=logInUser)
    loginBtn.place(x=85, y=520, width=360, height=80)

def showRegisterScreen():

    def registerUser():
        username = usernameInput.get()
        password = passwordInput.get()
        status = registerClient(username, password)

        if status == "failure":
            registerTitle.config(text='THAT USER IS\nALREADY REGISTERED')
        else:
            logInClient(username, password)
            registerTitle.place_forget()
            usernameLabel.place_forget()
            usernameInput.place_forget()
            passwordLabel.place_forget()
            passwordInput.place_forget()
            loginBtn.place_forget()
            showLoggedInScreen()

    registerTitle = Label(t, text='PLEASE INPUT\nNEW USERNAME\nAND PASSWORD', font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    registerTitle.place(x=60, y=30, width=420, height=140)

    usernameLabel = Label(t, text='USERNAME', font=('Fixedsys', 24, 'bold'), fg='snow', bg='lightsalmon4')
    usernameLabel.place(x=115, y=200, width=300, height=70)
    usernameInput = Entry(t, font=('Fixedsys', 24))
    usernameInput.place(x=115, y=280, width=300, height=50)

    passwordLabel = Label(t, text='PASSWORD', font=('Fixedsys', 24, 'bold'), fg='snow', bg='lightsalmon4')
    passwordLabel.place(x=115, y=360, width=300, height=70)
    passwordInput = Entry(t, font=('Fixedsys', 24), show='*')
    passwordInput.place(x=115, y=440, width=300, height=50)

    loginBtn = Button(t, text='REGISTER', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=registerUser)
    loginBtn.place(x=85, y=520, width=360, height=80)

def showLoggedInScreen(status = "normal"):
    
    global looking, lookingThread, cancelSearch
    looking = False
    lookingThread = None
    cancelSearch = threading.Event()

    def lookForMatch():
        global looking, lookingThread, cancelSearch
        if not looking:  
            play.place_forget()
            cancel.place(x=85, y=210, width=360, height=100)
            title.config(text="WAITING FOR\nMATCH!")
            looking = True  
            cancelSearch.clear()  
            lookingThread = threading.Thread(target=findMatch)  
            lookingThread.start()  
            print("Waiting for match to start...")

    def cancelLookingForMatch():
        global looking, cancelSearch
        if looking:  
            looking = False
            cancel.place_forget()
            play.place(x=85, y=210, width=360, height=100)
            title.config(text="IX OX DELUXE")
            cancelSearch.set()  
            leaveQueue()  
            if lookingThread and lookingThread.is_alive():  
                lookingThread.join()  
                print("Match search canceled.")
                cancelSearch.clear()  

    def logOutUser():
        global isLoggedIn
        cancelLookingForMatch()
        isLoggedIn = False
        play.place_forget()
        logOut.place_forget()
        cancel.place_forget()
        stats.place_forget()
        title.place_forget()
        returnToWelcomeScreen()

    def checkStats():
        cancelLookingForMatch()
        title.place_forget()
        play.place_forget()
        cancel.place_forget()
        stats.place_forget()
        logOut.place_forget()
        showStatsScreen()

    t.config(bg="linen")
    title = Label(t, text='IX OX DELUXE', font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    title.place(x=60, y=40, width=420, height=140)

    play = Button(t, text='PLAY', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=lookForMatch)
    play.place(x=85, y=210, width=360, height=100)

    cancel = Button(t, text='CANCEL WAITING\nFOR MATCH', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=cancelLookingForMatch)

    stats = Button(t, text='STATS', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=checkStats)
    stats.place(x=85, y=350, width=360, height=100)


    logOut = Button(t, text='LOG OUT', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=logOutUser)
    logOut.place(x=85, y=490, width=360, height=100)

def returnToWelcomeScreen():
    
    def goToLoginScreen():
        title.place_forget()
        login.place_forget()
        register.place_forget()
        server.place_forget()
        showLoginScreen()
    
    def goToRegisterScreen():
        title.place_forget()
        login.place_forget()
        register.place_forget()
        server.place_forget()
        showRegisterScreen()

    def goToServerScreen():
        title.place_forget()
        login.place_forget()
        register.place_forget()
        server.place_forget()
        showServerScreen()

    title = Label(t, text='IX OX DELUXE', font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    title.place(x=60, y=30, width=420, height=140)

    login = Button(t, text='LOGIN', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=goToLoginScreen)
    login.place(x=85, y=210, width=360, height=100)

    register = Button(t, text='REGISTER', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=goToRegisterScreen)
    register.place(x=85, y=350, width=360, height=100)

    server = Button(t, text='SERVER', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=goToServerScreen)
    server.place(x=85, y=490, width=360, height=100)

def showStatsScreen():

    def goBack():
        name.place_forget()
        wins.place_forget()
        ties.place_forget()
        losses.place_forget()
        back.place_forget()
        showLoggedInScreen()

    stats = statsClient(username)

    winsOut = "WINS: " + str(stats[0])
    tiesOut = "TIES: " + str(stats[1])
    lossesOut = "LOSSES: " + str(stats[2])
    
    name = Label(t, text=username, font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    name.place(x=60, y=30, width=420, height=100)

    wins = Label(t, text=winsOut, font=('Fixedsys', 32, 'bold'), fg='snow', bg='lightsalmon4')
    wins.place(x=75, y=160, width=390, height=90)

    ties = Label(t, text=tiesOut, font=('Fixedsys', 32, 'bold'), fg='snow', bg='lightsalmon4')
    ties.place(x=75, y=270, width=390, height=90)

    losses = Label(t, text=lossesOut, font=('Fixedsys', 32, 'bold'), fg='snow', bg='lightsalmon4')
    losses.place(x=75, y=380, width=390, height=90)

    back = Button(t, text='GO BACK', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=goBack)
    back.place(x=85, y=510, width=360, height=100)

def showGameScreen(board, fetchedCurrentPlayer, secondPlayer):
    global currentPlayer, buttons, clientSocket, username
    currentPlayer = fetchedCurrentPlayer
    symbol = 'X' if username == fetchedCurrentPlayer else 'O'

    if(username == currentPlayer):
        clientSocket.sendall(json.dumps({"action": "move", "move": 10}).encode())
        clientSocket.recv(bufferSize)

    for widget in t.winfo_children():
        widget.destroy()

    surrender = Button(t, text="SURRENDER!", font=('Fixedsys', 32, 'bold'), fg='snow', bg='red3', command=lambda : surrenderMatch())
    surrender.place(x=30, y=30, width=480, height=40)

    player1 = Label(t, text=currentPlayer, font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    player1.place(x=30, y=90, width=400, height=60)

    player2 = Label(t, text=secondPlayer, font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    player2.place(x=110, y=550, width=400, height=60)

    symbol1 = Label(t, text="X", font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed4')
    symbol1.place(x=430, y=90, width=80, height=60)

    symbol2 = Label(t, text="O", font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed4')
    symbol2.place(x=30, y=550, width=80, height=60)

    boardFrame = Frame(t, bg='IndianRed3')
    boardFrame.place(relx=0.5, rely=0.55, anchor=CENTER)

    t.config(bg='salmon')

    buttons = [[None, None, None], [None, None, None], [None, None, None]]

    def createButton(row, col):
        button = Button(boardFrame, text=" ", font=64, width=10, height=4, command=lambda r=row, c=col: handleClick(r, c))
        button.grid(row=row, column=col, padx=5, pady=5)
        buttons[row][col] = button

    def updateBoard(newBoard):
        for i in range(3):
            for j in range(3):
                buttons[i][j].config(text=newBoard[i * 3 + j], state='disabled' if newBoard[i * 3 + j] != ' ' else 'normal')

    for row in range(3):
        for col in range(3):
            createButton(row, col)

    if currentPlayer != username:
        for row in range(3):
            for col in range(3):
                buttons[row][col].config(state="disabled")

    def surrenderMatch():
        moveMessage = json.dumps({"action": "move", "move": 12, "surrenderingPlayer" : username})
        clientSocket.sendall(moveMessage.encode())
        surrender.config(state="disabled")
        for i in range(3):
            for j in range(3):
                buttons[i][j].config(state='disabled')

    def handleClick(row, col):
        if buttons[row][col]["text"] != "X" and  buttons[row][col]["text"] != "O" and currentPlayer == username:
            buttons[row][col]["text"] = symbol
            buttons[row][col].config(state="disabled")

            position = 1 + row * 3 + col
            moveMessage = json.dumps({"action": "move", "move": position})
            clientSocket.sendall(moveMessage.encode())
            print(f'Position: {position} => {row}, {col}')


    def listenForMove():
        try:
            while True:
                response = clientSocket.recv(bufferSize).decode()
                responseJson = json.loads(response)
                print(responseJson)

                if "status" in responseJson:
                    status = responseJson["status"]

                    if status == "prompt_move":
                        global currentPlayer
                        currentPlayer = responseJson["currentPlayer"]
                        if currentPlayer == username:
                            for row in range(3):
                                for col in range(3):
                                    buttons[row][col].config(state="normal")
                        else:
                            for row in range(3):
                                for col in range(3):
                                    buttons[row][col].config(state="disabled")

                    elif status == "move_success":
                        updateBoard(responseJson["board"])

                    elif status == "win" or status == "lose" or status == "draw":
                        for widget in t.winfo_children():
                            widget.destroy()
                        if "winningPlayer" in responseJson:
                            if responseJson["winningPlayer"] == username:
                                status = "YOU WON!"
                            else:
                                status = "YOU LOST!"
                        if "winningPlayerStats" in responseJson and "losingPlayerStats" in responseJson and "winningPlayer" in responseJson and "losingPlayer" in responseJson:
                            showResultScreen(status, responseJson["winningPlayerStats"], responseJson["losingPlayerStats"], responseJson["winningPlayer"], responseJson["losingPlayer"])
                        else:
                            status = "IT'S A TIE!"
                            showResultScreen(status, responseJson["player1Stats"], responseJson["player2Stats"], responseJson["player1"], responseJson["player2"])
                        break

                    elif status == "failure":
                        print("Invalid Move", responseJson["message"])
        
        except Exception as e:
            print(f"Error receiving move: {e}")
    threading.Thread(target=listenForMove).start()
              

def updateGameScreen(board, fetchedCurrentPlayer):
    global currentPlayer, buttons, isLoggedIn
    
    isLoggedIn = False
    currentPlayer = fetchedCurrentPlayer

    for row in range(3):
        for col in range(3):
            buttons[row][col]["text"] = board[row * 3 + col]
            buttons[row][col].config(state="normal" if board[row * 3 + col] == " " and currentPlayer == username else "disabled")

def showResultScreen(status, winningPlayerStats, losingPlayerStats, winningPlayer = "", losingPlayer = ""):

    breakRequest = {
            "action": "break",
            "username": username,
    }
    
    clientSocket.sendall(json.dumps(breakRequest).encode())

    t.config(bg="linen")

    winnerOut = winningPlayer + ": " + winningPlayerStats
    loserOut = losingPlayer + ": " + losingPlayerStats

    resultTitle = Label(t, text="GAME RESULT", font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    resultTitle.place(x=60, y=30, width=420, height=100)

    statusLabel = Label(t, text=status, font=('Fixedsys', 32, 'bold'), fg='snow', bg='lightsalmon4')
    statusLabel.place(x=75, y=160, width=390, height=90)

    winnerLabel = Label(t, text=winnerOut, font=('Fixedsys', 32, 'bold'), fg='snow', bg='lightsalmon4')
    winnerLabel.place(x=75, y=270, width=390, height=90)

    loserLabel = Label(t, text=loserOut, font=('Fixedsys', 32, 'bold'), fg='snow', bg='lightsalmon4')
    loserLabel.place(x=75, y=380, width=390, height=90)

    returnBtn = Button(t, text='GO BACK', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=reload)
    returnBtn.place(x=85, y=510, width=360, height=100)

def reload():

    for widget in t.winfo_children():
        widget.destroy()

    disconnect_thread = threading.Thread(target=disconnectFromServer, args=(clientSocket,))
    disconnect_thread.start()

    connectToServer(serverAddressPort)
    logInClient(username, password)
    showLoggedInScreen()

def showServerScreen(title = 'PLEASE INPUT\nIP\nAND PORT'):
    global serverAddressPort
    def saveServer():
        global serverAddressPort
        ipLabel.place_forget()
        ipInput.place_forget()
        portLabel.place_forget()
        portInput.place_forget()
        serverTitle.place_forget()
        saveBtn.place_forget()
        if clientSocket != None:
            disconnectFromServer(clientSocket)
        try:
            int(portInput.get())
        except:
            showServerScreen('CONNECTION FAILED')
        serverAddressPort = (ipInput.get(), int(portInput.get()))
        try:
            if (connectToServer(serverAddressPort) == 'failed'):
                showServerScreen('CONNECTION FAILED')
            else:
                returnToWelcomeScreen()
        except Exception as e:
                showServerScreen('CONNECTION FAILED')

    serverTitle = Label(t, text=title, font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    serverTitle.place(x=60, y=30, width=420, height=140)

    ipLabel = Label(t, text='IP', font=('Fixedsys', 24, 'bold'), fg='snow', bg='lightsalmon4')
    ipLabel.place(x=115, y=200, width=300, height=70)
    ipInput = Entry(t, font=('Fixedsys', 24))
    ipInput.place(x=115, y=280, width=300, height=50)

    portLabel = Label(t, text='PORT', font=('Fixedsys', 24, 'bold'), fg='snow', bg='lightsalmon4')
    portLabel.place(x=115, y=360, width=300, height=70)
    portInput = Entry(t, font=('Fixedsys', 24))
    portInput.place(x=115, y=440, width=300, height=50)

    saveBtn = Button(t, text='SAVE', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=saveServer)
    saveBtn.place(x=85, y=520, width=360, height=80)    
 
if __name__ == "__main__":

    def goToLoginScreen():
        title.place_forget()
        login.place_forget()
        register.place_forget()
        server.place_forget()
        showLoginScreen()
    
    def goToRegisterScreen():
        title.place_forget()
        login.place_forget()
        register.place_forget()
        server.place_forget()
        showRegisterScreen()

    def goToServerScreen():
        title.place_forget()
        login.place_forget()
        register.place_forget()
        server.place_forget()
        showServerScreen()

    t.title('IX OX DELUXE')
    t.config(bg='linen', width=540, height=640)

    title = Label(t, text='IX OX DELUXE', font=('Fixedsys', 32, 'bold'), fg='snow', bg='IndianRed3')
    title.place(x=60, y=30, width=420, height=140)

    login = Button(t, text='LOGIN', font=('Fixedsys', 24, 'bold'), state='disabled', fg='snow', bg='salmon', command=goToLoginScreen)
    login.place(x=85, y=210, width=360, height=100)

    register = Button(t, text='REGISTER', font=('Fixedsys', 24, 'bold'), state='disabled', fg='snow', bg='salmon', command=goToRegisterScreen)
    register.place(x=85, y=350, width=360, height=100)

    server = Button(t, text='SERVER', font=('Fixedsys', 24, 'bold'), fg='snow', bg='salmon', command=goToServerScreen)
    server.place(x=85, y=490, width=360, height=100)

    t.mainloop()

